<?php
class NBT_Price_Matrix_Settings{

	protected static $initialized = false;

	public static function initialize() {
		// Do nothing if pluggable functions already initialized.
		if ( self::$initialized ) {
			return;
		}


		// State that initialization completed.
		self::$initialized = true;
	}

    public static function get_settings() {
        $settings = array(
            'show_on' => array(
                'name' => __( 'Show on', 'stack-faqs' ),
                'type' => 'select',
                'id'   => 'wc_'.NBT_Solutions_Price_Matrix::$plugin_id.'_show_on',
                'options'       => array(
                    'default'   => __( 'Default', 'wps' ),
                    'before_tab'    => __( 'Before Tab', 'wps' )
                ),
                'default' => 'default'
            ),
            'hide_info' => array(
                'name' => __( 'Hide Additional information', 'stack-faqs' ),
                'type' => 'checkbox',
                'id'   => 'wc_'.NBT_Solutions_Price_Matrix::$plugin_id.'_hide_info',
                'default' => false,
                'label' => __('Settings to hide Additional information tab in Product Details', 'core-solutions')
            ),
            'is_heading' => array(
                'name' => __( 'Enable heading', 'stack-faqs' ),
                'type' => 'checkbox',
                'id'   => 'wc_'.NBT_Solutions_Price_Matrix::$plugin_id.'_is_heading',
                'default' => false,
                'label' => __('Turn on heading before Price Matrix table', 'core-solutions')                
            ),
            'is_scroll' => array(
                'name' => __( 'Scroll when select price', 'stack-faqs' ),
                'type' => 'checkbox',
                'id'   => 'wc_'.NBT_Solutions_Price_Matrix::$plugin_id.'_is_scroll',
                'default' => false,
                'label' => __('Scroll the screen to the Price Matrix table when user choose attributes', 'core-solutions')                                
            ),
            'heading_label' => array(
                'name' => __( 'Heading title', 'stack-faqs' ),
                'type' => 'text',
                'id'   => 'wc_'.NBT_Solutions_Price_Matrix::$plugin_id.'_heading',
                'default' => ''
            ),

            'is_show_sales' => array(
                'name' => __( 'Display regular & sale price', 'stack-faqs' ),
                'type' => 'checkbox',
                'id'   => 'wc_'.NBT_Solutions_Price_Matrix::$plugin_id.'_show_sales',
                'default' => false,
                'label' => __('Display the sale price in the Price Matrix table', 'core-solutions')                                
            ),
            array(
                'type' => 'border'
            ),
            'table_bg' => array(
                'name' => __( 'Background Table', 'stack-faqs' ),
                'type' => 'color',
                'id'   => 'wc_'.NBT_Solutions_Price_Matrix::$plugin_id.'_color_table',
                'default' => '#efefef',
            ),
            'table_color' => array(
                'name' => __( 'Color text', 'stack-faqs' ),
                'type' => 'color',
                'id'   => 'wc_'.NBT_Solutions_Price_Matrix::$plugin_id.'_color_text',
                'default' => '#333'
            ),
            'border_color' => array(
                'name' => __( 'Color border', 'stack-faqs' ),
                'type' => 'color',
                'id'   => 'wc_'.NBT_Solutions_Price_Matrix::$plugin_id.'_color_border',
                'default' => '#ccc'
            ),
            array(
                'type' => 'border'
            ),
            'bg_tooltip' => array(
                'name' => __( 'Background Tooltips', 'stack-faqs' ),
                'type' => 'color',
                'id'   => 'wc_'.NBT_Solutions_Price_Matrix::$plugin_id.'_bg_tooltip',
                'default' => '#efefef'
            ),
            'color_tooltip' => array(
                'name' => __( 'Color Tooltips', 'stack-faqs' ),
                'type' => 'color',
                'id'   => 'wc_'.NBT_Solutions_Price_Matrix::$plugin_id.'_color_tooltip',
                'default' => '#333'
            ),
            'border_tooltip' => array(
                'name' => __( 'Border Tooltips', 'stack-faqs' ),
                'type' => 'color',
                'id'   => 'wc_'.NBT_Solutions_Price_Matrix::$plugin_id.'_border_tooltip',
                'default' => '#ccc'
            ),
            'font_size' => array(
                'name' => __( 'Font Size', 'stack-faqs' ),
                'type' => 'number',
                'desc' => 'px',
                'id'   => 'wc_'.NBT_Solutions_Price_Matrix::$plugin_id.'_font_size',
                'default' => 14,
                'min' => 14,
                'max' => 50,
                'step' => 1
            )
        );
        return apply_filters( 'nbt_'.NBT_Solutions_Price_Matrix::$plugin_id.'_settings', $settings );
    }
}
