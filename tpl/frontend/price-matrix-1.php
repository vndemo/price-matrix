<?php
if(isset($_pm_attr['vertical'])){
    $attribute_one = pm_attribute_label($_pm_attr['vertical'][0], $product->get_id());
}else{
    $attribute_one = pm_attribute_label($_pm_attr['horizontal'][0], $product->get_id());
}?>
<div class="table-responsive">
    <table class="pure-table table-matrix-1">
        <tbody>

            <tr>
                <?php foreach ($attribute_one as $kat => $attr_one) :?>
                <td class="attr-name heading-center"><?php echo $attr_one->name;?></td>
                <?php endforeach;?>
            </tr>

            <tr>
                <?php foreach ($attribute_one as $kat => $attr_one) :
                    $hover_detail = '';
                    $group_attr = array(
                        array(
                            'name' => $attr_one->taxonomy,
                            'value' => $attr_one->slug
                        )
                    );

                    if($deprived){
                        $group_attr = array_merge($group_attr, $deprived);
                    }

                    $hover_detail .= '<tr><td>'.pm_attribute_tax($attr_one->taxonomy, $product->get_id()).'</td><td>'.$attr_one->name.'</td></tr>';
                     $show_regular_price = '';
                    if(isset($pm_settings['wc_price-matrix_show_sales']) && $pm_settings['wc_price-matrix_show_sales'] == 1){
                        $show_regular_price = ' show';
                    }

                    if(!$_price = str_replace(array($symbol, '.00'), '', pm_attribute_price($group_attr, $product->get_id(), true))){
                        $_price = '&nbsp;';
                    }


                ?>
                <td class="price tippy" title="<table><?php echo $hover_detail;?></table>" data-attr="<?php echo htmlspecialchars(wp_json_encode( $group_attr ));?>"><span class="pm-price-wrap<?php echo $show_regular_price;?>"><?php echo pm_attribute_price($group_attr, $product->get_id());?></span></td>
                <?php endforeach;?>
            </tr>
        </tbody>
    </table>
</div>