<?php
if(isset($_pm_attr['vertical'])){
    $attribute_one = pm_attribute_label($_pm_attr['vertical'][0], $product_id);
}else{
    $attribute_one = pm_attribute_label($_pm_attr['horizontal'][0], $product_id);
}?>
<div class="table-responsive">
    <table class="pure-table price-matrix-table" style="overflow: inherit;">
        <tbody>
            <tr>
                <?php foreach ($attribute_one as $kat => $attr_one) :?>
                <td class="attr-name heading-center"><?php echo $attr_one->name;?></td>
                <?php endforeach;?>
            </tr>

            <tr>
                <?php foreach ($attribute_one as $kat => $attr_one) :
                    $group_attr = array(
                        array(
                            'name' => $attr_one->taxonomy,
                            'value' => $attr_one->slug
                        )
                    );

                    if($deprived){
                        $group_attr = array_merge($group_attr, $deprived);
                    }

                    if(!$_price = str_replace(array($symbol, '.00'), '', pm_attribute_price($group_attr, $product->get_id(), true))){
                        $_price = '&nbsp;';
                    }


                ?>
                <td class="price" data-attr="<?php echo htmlspecialchars(wp_json_encode( $group_attr ));?>"><div class="wrap"><div style="margin: 10px 5px;" class="" contenteditable="false"><?php echo $_price;?></div></div></td>
                <?php endforeach;?>
            </tr>
        </tbody>
    </table>
    <input type="hidden" name="security" value="<?php echo wp_create_nonce( "_price_matrix_save" );?>" />
    <button type="button" class="button save_enter_price button-primary" style="margin-top: 15px;">Save</button>
    <span class="loading-wrap"><span class="enter-price-loading"></span></span>
</div>
