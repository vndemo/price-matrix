<?php

$attribute_one = pm_attribute_label($_pm_attr['horizontal'][0], $product->get_id());
$attribute_two = pm_attribute_label($_pm_attr['horizontal'][1], $product->get_id());
$attribute_three = pm_attribute_label($_pm_attr['vertical'][0], $product->get_id());
?>
<div class="table-responsive">
    <table class="pure-table price-matrix-table horizontal-3">
        <tbody>
            <tr>
                <td class="attr-name colspan" rowspan="2"></td>
                <?php foreach ($attribute_one as $kat => $attr_one) :?>
                <td class="attr-name heading-center" colspan="<?php echo count($attribute_one);?>"><?php echo $attr_one->name;?></td>
                <?php endforeach;?>
            </tr>
            <?php
            end($attribute_one);
            $last_one = key($attribute_one);
            foreach ($attribute_one as $k_one => $attr_one) :
            if($k_one == 0):
                echo '<tr>';
            endif;?>
                <?php foreach ($attribute_two as $k_two => $attr_two) :?>
                <td class="attr-name"><?php echo $attr_two->name;?></td>
                <?php endforeach;?>
     
            <?php
            if($k_one == $last_one):
                echo '</tr>';
            endif;
            endforeach;?>

     
            <?php foreach ($attribute_three as $k_three => $attr_three) :?>
            <tr>
                <td class="attr-name"><?php echo $attr_three->name;?></td>
                <?php
                foreach ($attribute_one as $k_one => $attr_one) :
                    foreach ($attribute_two as $k_two => $attr_two) :
                        $hover_detail = '';
                        $group_attr = array(
                            array(
                                'name' => $attr_one->taxonomy,
                                'value' => $attr_one->slug
                            ),
                            array(
                                'name' => $attr_two->taxonomy,
                                'value' => $attr_two->slug
                            ),
                            array(
                                'name' => $attr_three->taxonomy,
                                'value' => $attr_three->slug
                            )
                        );
                        if($deprived){
                            $group_attr = array_merge($group_attr, $deprived);
                        }
                        $hover_detail .= '<tr><td>'.pm_attribute_tax($attr_one->taxonomy, $product->get_id()).'</td><td>'.$attr_one->name.'</td></tr>';
                        $hover_detail .= '<tr><td>'.pm_attribute_tax($attr_two->taxonomy, $product->get_id()).'</td><td>'.$attr_two->name.'</td></tr>';
                        $hover_detail .= '<tr><td>'.pm_attribute_tax($attr_three->taxonomy, $product->get_id()).'</td><td>'.$attr_three->name.'</td></tr>';
                    ?>
                    <td class="price tippy" title="<table><?php echo $hover_detail;?></table>" data-attr="<?php echo htmlspecialchars(wp_json_encode( $group_attr ));?>"><span class="pm-price-wrap"><?php echo pm_attribute_price($group_attr, $product->get_id());?></span></td>
                    <?php endforeach;
                endforeach;?>

            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</div>