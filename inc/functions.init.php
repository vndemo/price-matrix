<?php
	function wc_get_product_direction_pm_options(){
		return array(
			'default'    => __( 'Default', 'woocommerce' ),
			'before_tab' => __( 'Before Tab', 'woocommerce' ),
		);
	}

	/**
	 * Price Matrix: Attribute Label
	 *
	 * Shows label with attribute keys
	 */
	function pm_attribute_label($tax, $product_id){
		$product = wc_get_product($product_id);
		$get_attributes = $product->get_attributes( 'edit' );


		if(isset($get_attributes[$tax])) :
			$attribute = $get_attributes[$tax];


			if ( $attribute->is_taxonomy() ) :
				$array = $attribute->get_terms();
			else :
				$value_array = $attribute->get_options();
				$array = array();

				foreach ($value_array as $key => $value) {
					$array[] = array(
						'taxonomy' => $tax,
						'name' => trim($value),
						'slug' => trim($value),
						'is_taxonomy' => false
					);
				}
				$array = json_decode(json_encode($array), FALSE);
			endif;
		endif;

		usort($array,function($first,$second){
		    return $first->name > $second->name;
		});


		return $array;
		
	}

function cmp($a, $b)
{
    return strcmp($a->name, $b->name);
}
	/**
	 * Price Matrix: Attribute Label
	 *
	 * Shows label with attribute keys
	 */
	function pm_attribute_tax($tax, $product_id){
		global $wpdb;

		$_product_attributes = get_post_meta($product_id, '_product_attributes', TRUE);
		if(isset($_product_attributes[$tax])){
			$data = $_product_attributes[$tax];
			if($data['is_taxonomy']){
				$tax = str_replace('pa_', '', $tax);
				
				$rs =  $wpdb->get_row($wpdb->prepare("SELECT attribute_label FROM ".$wpdb->prefix."woocommerce_attribute_taxonomies WHERE attribute_name = '%s'", $tax), OBJECT);
				if($rs){
					return $rs->attribute_label;
				}
			}else{
				return $data['name'];
			}
		}
	}

	/**
	 * Price Matrix: Attribute Price
	 *
	 * Shows price with attribute
	 */
	function pm_attribute_price($array = array(), $post_id, $ajax = false){
		global $wpdb;

		$sql = "SELECT posts.ID as id, posts.post_parent as parent FROM {$wpdb->posts} as posts";
		if(is_array($array) && !empty($array)){
			foreach ($array as $k => $value) {
				$sql .= " INNER JOIN {$wpdb->postmeta} AS postmeta".$k." ON posts.ID = postmeta".$k.".post_id";
			}
		}
		$sql .= " WHERE posts.post_parent = '".$post_id."' AND posts.post_type IN ( 'product', 'product_variation' ) AND posts.post_status = 'publish'";
		$sqls = "";
		if(is_array($array) && !empty($array)){
			$i = 1;
			foreach ($array as $key => $val_attr) {
				$sql .= " AND postmeta".$key.".meta_key = 'attribute_".sanitize_text_field($val_attr['name'])."' AND postmeta".$key.".meta_value = '". sanitize_text_field($val_attr['value']) ."'";
				$sqls .= " AND postmeta".$key.".meta_key = 'attribute_".sanitize_text_field($val_attr['name'])."' AND postmeta".$key.".meta_value = '". sanitize_text_field($val_attr['value']) ."'";
				$i++;
			}
		}
		$sql .= " GROUP BY posts.ID ORDER BY posts.post_title DESC";


		$items = $wpdb->get_row($sql);
		if($items){
			$_product = wc_get_product( $items->id );

			$_product = wc_get_product( $items->id );
			$symbol = get_woocommerce_currency_symbol(get_option('woocommerce_currency'));
			if(preg_match("'<del>(.*?)</del>'si", $_product->get_price_html(), $matches) && $ajax){
				$del = str_replace(array($symbol, '.00'), '', $matches[1]);

				preg_match("'<ins>(.*?)</ins>'si", $_product->get_price_html(), $ins_matches);
				$ins = str_replace(array($symbol, '.00'), '', $ins_matches[1]);
				return $del.'-'.$ins;
			}



			return $_product->get_price_html();			
		}else{
			return false;
		}
	}


	function get_by_name($products, $field){
		foreach ($array as $key => $value) {
			if($value['name'] == $field){
				return $key;
			}
		}
	}

?>